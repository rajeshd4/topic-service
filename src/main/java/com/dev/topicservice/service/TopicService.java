/**
 * 
 */
package com.dev.topicservice.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.dev.topicservice.domain.Topic;
import com.dev.topicservice.repository.TopicRepository;

/**
 * @author devendra.singh
 *
 */
@Service
public class TopicService {

	/**
	 * 
	 */

	@Autowired
	TopicRepository topicRepository;

	@Autowired
	private MongoTemplate mongoTemplate;

	public TopicService() {
		// TODO Auto-generated constructor stub
	}

	public List<Topic> getAllTopic() {
		// TODO Auto-generated method stub

		// return topicList;
		return topicRepository.findAll();
	}

	public List<Topic> getTopicsByUserid(String userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("userid").is(userId));
		return mongoTemplate.find(query, Topic.class);
	}

	public Optional<Topic> getTopicById(String id) {

		return topicRepository.findById(id);
	}

	public void addTopic(Topic topic) {
		// TODO Auto-generated method stub
		System.out.println(" Adding topics in topicList.");
		// topicList.add(topic);
		topicRepository.save(topic);
	}

	public void updateTopic(Topic topic, String id) {
		// TODO Auto-generated method stub
		topic.setId(id);
		topicRepository.save(topic);

	}

	public void deleteTopic(String id) {
		// TODO Auto-generated method stub
		// topicList.removeIf(topic -> topic.getId().equals(id));
		topicRepository.deleteById(id);
	}

}
