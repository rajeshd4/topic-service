/**
 * 
 */
package com.dev.topicservice.domain;


/**
 * @author devendra.singh
 *
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "topic")
public class Topic {

	/**
	 * 
	 */
	@Id
	private String id;
	private String name;
    private String description;
    private String userid;
    
    
    


	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public Topic() {
		// TODO Auto-generated constructor stub
	}
	
	public Topic(String id, String name, String description,String userid) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.userid = userid;
       
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    @Override
    public String toString() {
      return "Topic [id=" + id + ", name=" + name + ", description=" + description + ", userid=" + userid + "]";
    }

}

