/**
 * 
 */
package com.dev.topicservice.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.dev.topicservice.domain.Topic;

/**
 * @author devendra.singh
 *
 */
@Repository
public interface TopicRepository extends MongoRepository<Topic, String> {
	
	public List<Topic> getTopicsByUserid(String userId);

}
