/**
 * 
 */
package com.dev.topicservice.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.dev.topicservice.domain.Topic;
import com.dev.topicservice.service.TopicService;

/**
 * @author devendra.singh
 *
 */
@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RequestMapping(value = "/api")
public class TopicController {

	/**
	 * 
	 */

	@Autowired
	TopicService topicService;

	@RequestMapping("/topics")
	public List<Topic> allTopics() throws Exception {
		System.out.println("In AllTopics............");
		return topicService.getAllTopic();
	}

	@RequestMapping("/topics/{id}")
	public Optional<Topic> getTopicById(@PathVariable("id") String id) {

		return topicService.getTopicById(id);
	}

	@RequestMapping(value = "/topics", method = RequestMethod.POST)
	public void addTopics(@RequestBody Topic topic) {
		System.out.println("in add topic");
		topicService.addTopic(topic);
	}

	@RequestMapping(value = "/topics/{id}", method = RequestMethod.PUT)
	public void updateTopics(@RequestBody Topic topic, @PathVariable("id") String id) {

		topicService.updateTopic(topic, id);
	}

	@RequestMapping(value = "/topics/{id}", method = RequestMethod.DELETE)
	public void deleteTopics(@PathVariable("id") String id) {

		topicService.deleteTopic(id);
	}

	/////// Base on userid get Topics ///////
	@RequestMapping("/topics/user/{userId}")
	public List<Topic> getTopicByUserId(@PathVariable("userId") String userId) {

		return topicService.getTopicsByUserid(userId);

	}
}
