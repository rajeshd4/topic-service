FROM williamyeh/java8
EXPOSE 9999
ENTRYPOINT [pwd]
RUN ls -la
COPY target/*.jar topic-service.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/topic-service.jar"]
